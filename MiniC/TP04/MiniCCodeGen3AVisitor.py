from typing import List, Tuple
from MiniCVisitor import MiniCVisitor
from MiniCParser import MiniCParser
from Lib.LinearCode import LinearCode
from Lib import RiscV
from Lib.RiscV import Condition
from Lib import Operands
from antlr4.tree.Trees import Trees
from Lib.Errors import MiniCInternalError, MiniCUnsupportedError

"""
CAP, MIF08, three-address code generation + simple alloc
This visitor constructs an object of type "LinearCode".
"""


class MiniCCodeGen3AVisitor(MiniCVisitor):

    _current_function: LinearCode

    def __init__(self, debug, parser):
        super().__init__()
        self._parser = parser
        self._debug = debug
        self._functions = []
        self._lastlabel = ""

    def get_functions(self) -> List[LinearCode]:
        return self._functions

    def printSymbolTable(self):  # pragma: no cover
        print("--variables to temporaries map--")
        for keys, values in self._symbol_table.items():
            print(keys + '-->' + str(values))

    # handle variable decl

    def visitVarDecl(self, ctx) -> None:
        type_str = ctx.typee().getText()
        vars_l = self.visit(ctx.id_l())
        for name in vars_l:
            if name in self._symbol_table:
                raise MiniCInternalError(
                    "Variable {} has already been declared".format(name))
            else:
                tmp = self._current_function.fdata.fresh_tmp()
                self._symbol_table[name] = tmp
                if type_str not in ("int", "bool"):
                    raise MiniCUnsupportedError("Unsupported type " + type_str)
                # Initialization to 0 or False, both represented with 0
                self._current_function.add_instruction(
                    RiscV.li(tmp, Operands.Immediate(0)))

    def visitIdList(self, ctx) -> List[str]:
        t = self.visit(ctx.id_l())
        t.append(ctx.ID().getText())
        return t

    def visitIdListBase(self, ctx) -> List[str]:
        return [ctx.ID().getText()]

    # expressions

    def visitParExpr(self, ctx) -> Operands.Temporary:
        return self.visit(ctx.expr())

    def visitIntAtom(self, ctx) -> Operands.Temporary:
        val = Operands.Immediate(int(ctx.getText()))
        dest_temp = self._current_function.fdata.fresh_tmp()
        self._current_function.add_instruction(RiscV.li(dest_temp, val))
        return dest_temp

    def visitFloatAtom(self, ctx) -> Operands.Temporary:
        raise MiniCUnsupportedError("float literal")

    def visitBooleanAtom(self, ctx) -> Operands.Temporary:
        bool_str = ctx.getText()  # Récupérer le texte du contexte
        if bool_str == 'true':  # Vérifier si le texte est "true"
            val = Operands.Immediate(1)  # Créer une constante immédiate de valeur 1
        elif bool_str == 'false':  # Vérifier si le texte est "false"
            val = Operands.Immediate(0)  # Créer une constante immédiate de valeur 0
        else:  # Si le texte n'est pas "true" ou "false", lever une exception
            raise MiniCUnsupportedError("not a bool")
        dest_temp = self._current_function.fdata.fresh_tmp()  # Créer un nouveau Temporary pour stocker le résultat
        self._current_function.add_instruction(RiscV.li(dest_temp, val))  # Charger la constante immédiate dans le Temporary
        return dest_temp  # Renvoyer le Temporary



    def visitIdAtom(self, ctx) -> Operands.Temporary:
        try:
            # get the temporary associated to id
            return self._symbol_table[ctx.getText()]
        except KeyError:  # pragma: no cover
            raise MiniCInternalError(
                "Undefined variable {}, this should have failed to typecheck."
                .format(ctx.getText())
            )

    def visitStringAtom(self, ctx) -> Operands.Temporary:
        raise MiniCUnsupportedError("string atom")

    # now visit expressions

    def visitAtomExpr(self, ctx) -> Operands.Temporary:
        return self.visit(ctx.atom())

    def visitAdditiveExpr(self, ctx) -> Operands.Temporary:
        assert ctx.myop is not None  # Vérifie que le noeud possède un opérateur
        lval = self.visit(ctx.expr(0))  # Visite le noeud de l'expression gauche
        rval = self.visit(ctx.expr(1))  # Visite le noeud de l'expression droite
        dest_temp = self._current_function.fdata.fresh_tmp()  # Crée un nouveau temporaire pour stocker le résultat
        if ctx.myop.type == MiniCParser.PLUS:  # Si l'opérateur est un "+"
            self._current_function.add_instruction(RiscV.add(dest_temp, lval, rval))  # Ajoute une instruction d'addition
        else:  # Sinon (l'opérateur est un "-")
            self._current_function.add_instruction(RiscV.sub(dest_temp, lval, rval))  # Ajoute une instruction de soustraction
        return dest_temp # Renvoyer le Temporary




    def visitOrExpr(self, ctx) -> Operands.Temporary:
        lval = self.visit(ctx.expr(0)) # évalue l'expression gauche
        rval = self.visit(ctx.expr(1)) # évalue l'expression droite
        dest_temp = self._current_function.fdata.fresh_tmp() # crée un nouveau Temporary pour stocker le résultat
        self._current_function.add_instruction(RiscV.lor(dest_temp, lval, rval)) # ajoute l'instruction "lor" pour le OR logique et stocke le résultat dans le Temporary
        return dest_temp # Renvoyer le Temporary



    def visitAndExpr(self, ctx) -> Operands.Temporary:
        lval = self.visit(ctx.expr(0))  # Visite l'expression gauche
        rval = self.visit(ctx.expr(1))  # Visite l'expression droite
        dest_temp = self._current_function.fdata.fresh_tmp()  # Crée un nouvel enregistrement temporaire
        self._current_function.add_instruction(RiscV.land(dest_temp, lval, rval))  # Ajoute une instruction qui fait un AND logique des deux expressions et stocke le résultat dans l'enregistrement temporaire
        return dest_temp  # Renvoyer le Temporary


    def visitEqualityExpr(self, ctx) -> Operands.Temporary:
        return self.visitRelationalExpr(ctx)

    def visitRelationalExpr(self, ctx) -> Operands.Temporary:
        assert ctx.myop is not None  
        c = Condition(ctx.myop.type)  
        if self._debug:
            print("relational expression:")
            print(Trees.toStringTree(ctx, None, self._parser))
            print("Condition:", c)
        dest_temp = self._current_function.fdata.fresh_tmp()  # Obtient un nouveau Temporary pour stocker le résultat
        lval = self.visit(ctx.expr(0))  # Visite l'expression de gauche
        rval = self.visit(ctx.expr(1))  # Visite l'expression de droite
        end_rela = self._current_function.fdata.fresh_label("end_rela")  # Obtient un nouveau label
        self._current_function.add_instruction(RiscV.li(dest_temp, Operands.Immediate(0)))  # Initialise la valeur à False
        self._current_function.add_instruction(RiscV.conditional_jump(end_rela, lval, c.negate(), rval))  # Saute à la fin de la comparaison si la condition n'est pas vérifiée
        self._current_function.add_instruction(RiscV.li(dest_temp, Operands.Immediate(1)))  # Modifie la valeur à True
        self._current_function.add_label(end_rela)  # Ajoute le label à la fin de la comparaison
        return dest_temp  # Renvoyer le Temporary



    def visitMultiplicativeExpr(self, ctx) -> Operands.Temporary:
        assert ctx.myop is not None  
        div_by_zero_lbl = self._current_function.fdata.get_label_div_by_zero()  

        dest_temp = self._current_function.fdata.fresh_tmp()  # Alloue un nouveau registre temporaire pour stocker le résultat
        lval = self.visit(ctx.expr(0))  # Évalue l'expression de gauche
        rval = self.visit(ctx.expr(1))  # Évalue l'expression de droite

        if ctx.myop.type == MiniCParser.MULT:  # Si l'opération est une multiplication
            self._current_function.add_instruction(RiscV.mul(dest_temp, lval, rval))  # Ajoute l'instruction de multiplication à la fonction courante
        else:  # Sinon, l'opération est une division ou un modulo
            self._current_function.add_instruction(RiscV.conditional_jump(div_by_zero_lbl, rval, Condition('beq'), Operands.ZERO))  # Ajoute l'instruction de saut conditionnel pour éviter une division par zéro
            if ctx.myop.type == MiniCParser.DIV:  # Si l'opération est une division
                self._current_function.add_instruction(RiscV.div(dest_temp, lval, rval))  # Ajoute l'instruction de division à la fonction courante
            elif ctx.myop.type == MiniCParser.MOD:  # Si l'opération est un modulo
                self._current_function.add_instruction(RiscV.rem(dest_temp, lval, rval))  # Ajoute l'instruction de modulo à la fonction courante

        return dest_temp  # Renvoyer le Temporary


    def visitNotExpr(self, ctx) -> Operands.Temporary:
        expr_temp = self.visit(ctx.expr())  # évalue l'expression à nier
        dest_temp = self._current_function.fdata.fresh_tmp()  # récupère un registre temporaire pour stocker le résultat
        self._current_function.add_instruction(RiscV.xor(dest_temp, expr_temp, Operands.Immediate(1)))  # effectue un OU exclusif entre l'expression évaluée et la valeur 1 (ce qui revient à inverser les bits)
        return dest_temp # Renvoyer le Temporary



    def visitUnaryMinusExpr(self, ctx) -> Operands.Temporary:
        expr_temp = self.visit(ctx.expr())  # Visite l'expression et stocke le résultat dans expr_temp
        dest_temp = self._current_function.fdata.fresh_tmp()  # Crée un nouveau registre temporaire pour stocker le résultat final
        self._current_function.add_instruction(RiscV.sub(dest_temp, Operands.ZERO, expr_temp))  # Ajoute une instruction pour effectuer la soustraction de 0 et de l'expression visitée, et stocke le résultat dans dest_temp
        return dest_temp # Renvoyer le Temporary


    def visitProgRule(self, ctx) -> None:
        self.visitChildren(ctx)

    def visitFuncDef(self, ctx) -> None:
        funcname = ctx.ID().getText()
        self._current_function = LinearCode(funcname)
        self._symbol_table = dict()

        self.visit(ctx.vardecl_l())
        self.visit(ctx.block())
        self._current_function.add_comment("Return at end of function:")
        # This skeleton doesn't deal properly with functions, and
        # hardcodes a "return 0;" at the end of function. Generate
        # code for this "return 0;".
        self._current_function.add_instruction(
        	RiscV.li(Operands.A0, Operands.Immediate(0)))
        self._functions.append(self._current_function)
        del self._current_function

    def visitAssignStat(self, ctx) -> None:
        if self._debug:
            print("assign statement, rightexpression is:")
            print(Trees.toStringTree(ctx.expr(), None, self._parser))
        expr_temp = self.visit(ctx.expr())
        name = ctx.ID().getText()
        self._current_function.add_instruction(RiscV.mv(self._symbol_table[name], expr_temp))

    def visitIfStat(self, ctx) -> None:
        if self._debug:
            print("if statement")
        end_if_label = self._current_function.fdata.fresh_label("end_if")
        
        else_label = self._current_function.fdata.fresh_label("else") # Crée une nouvelle étiquette pour la branche else
        self._current_function.add_instruction(RiscV.conditional_jump(else_label, self.visit(ctx.expr()), Condition("beq"), Operands.ZERO)) # Ajoute une instruction pour sauter vers le label else si la condition est fausse
        self.visit(ctx.then_block) # Visite le bloc then de l'instruction if
        self._current_function.add_instruction(RiscV.jump(end_if_label)) # Ajoute une instruction pour sauter vers la fin de la structure if
        self._current_function.add_label(else_label) # Ajoute le label pour la branche else
        if ctx.else_block is not None: # Vérifie s'il y a une branche else dans l'instruction if
            self.visit(ctx.else_block) # Si oui, visite le bloc else
            
        self._current_function.add_label(end_if_label) 


    def visitWhileStat(self, ctx) -> None:
        if self._debug:  
            print("while statement, condition is:")  
            print(Trees.toStringTree(ctx.expr(), None, self._parser))  
            print("and block is:")  
            print(Trees.toStringTree(ctx.stat_block(), None, self._parser)) 

        loop_label = self._current_function.fdata.fresh_label("loop")  # Création d'un label pour la boucle while
        end_loop_label = self._current_function.fdata.fresh_label("end_loop")  # Création d'un label pour la fin de la boucle while
        self._current_function.add_label(loop_label)  # Ajout du label de la boucle au code généré
        self._current_function.add_instruction(RiscV.conditional_jump(end_loop_label, self.visit(ctx.expr()), Condition("beq"), Operands.ZERO))  # Ajout d'une instruction de saut conditionnel pour sortir de la boucle
        self.visit(ctx.stat_block())  # Visite du bloc d'instructions dans la boucle while
        self._current_function.add_instruction(RiscV.jump(loop_label))  # Ajout d'une instruction de saut pour retourner au début de la boucle
        self._current_function.add_label(end_loop_label)  # Ajout du label de fin de boucle au code généré


    def visitPrintlnintStat(self, ctx) -> None:
        expr_loc = self.visit(ctx.expr())
        if self._debug:
            print("print_int statement, expression is:")
            print(Trees.toStringTree(ctx.expr(), None, self._parser))
        self._current_function.add_instruction_PRINTLN_INT(expr_loc)

    def visitPrintlnboolStat(self, ctx) -> None:
        expr_loc = self.visit(ctx.expr())
        self._current_function.add_instruction_PRINTLN_INT(expr_loc)

    def visitPrintlnfloatStat(self, ctx) -> None:
        raise MiniCUnsupportedError("Unsupported type float")

    def visitPrintlnstringStat(self, ctx) -> None:
        raise MiniCUnsupportedError("Unsupported type string")

    def visitStatList(self, ctx) -> None:
        for stat in ctx.stat():
            self._current_function.add_comment(Trees.toStringTree(stat, None, self._parser))
            self.visit(stat)

from Lib import RiscV
from Lib.Operands import GP_REGS, DataLocation, Offset, Temporary, Operand, S
from Lib.Statement import Instruction
from Lib.Allocator import Allocator
from typing import List, Dict


class HybridNaiveAllocator(Allocator):

    def replace(self, old_instr: Instruction) -> List[Instruction]:
        """Replace Temporary operands with the corresponding allocated
        physical register (Register) OR memory location (Offset)."""
        numreg = 1
        before: List[Instruction] = []
        after: List[Instruction] = []
        subst: Dict[Operand, Operand] = {}

        old_args = old_instr.args()  # Récupère les arguments de l'instruction à remplacer
        for arg in old_args:  # Pour chaque argument de l'instruction
            if isinstance(arg, Temporary):  # Si c'est un Temporary
                loc = arg.get_alloced_loc()  # Récupère son emplacement alloué
                if isinstance(loc, Offset):  # Si c'est un Offset
                    regxxx = S[numreg]  # Récupère le registre pour stocker la valeur
                    numreg += 1  
                    before.append(RiscV.ld(regxxx, loc))  # Charge la valeur de l'emplacement Offset dans le registre
                    after.append(RiscV.sd(regxxx, loc))  # Stocke la valeur du registre dans l'emplacement Offset
                    subst[arg] = regxxx  # Ajoute l'association Temporary -> registre dans la substitution
                else:  
                    subst[arg] = loc  # Ajoute l'association Temporary -> emplacement dans la substitution



        new_instr = old_instr.substitute(subst)
        return before + [new_instr] + after
    

    def prepare(self) -> None:
        """Allocate all temporaries to registers first (like Naive), and then
        memory (like AllInMem).
        Invariants: - Expanded instructions can use s1, s2 and s3
          (to store the values of temporaries before the actual instruction).
        """
        regs = list(GP_REGS)  # Get a writable copy
        temp_allocation: Dict[Temporary, DataLocation] = dict()
        for tmp in self._fdata._pool.get_all_temps():
            if regs:
                location = regs.pop()
            else:
                location = self._fdata.fresh_offset()
            temp_allocation[tmp] = location
        self._fdata._pool.set_temp_allocation(temp_allocation)

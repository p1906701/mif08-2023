#include "printlib.h"

int main() {
    
    int x,y;
    x = 42;
    println_int(-x);
    println_int(-4*2);
    println_int(-7);
    println_int(-10*-8);
    println_int(-2*(2+3));
    println_int(-(-4));
    return 0;
}

// EXPECTED
// -42
// -8
// -7
// 80
// -10
// 4 
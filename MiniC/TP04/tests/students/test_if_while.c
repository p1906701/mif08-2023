#include "printlib.h"

int main()
{
  int a,b;
  a = 10;
  b = 0;
  if (a > 8) {
    while (b < 15) {
      b = b + a;
    }
  }
  println_int(b);
  return 0;
}

// EXPECTED
// 20



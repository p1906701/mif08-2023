#include "printlib.h"

int main() {
    
    int x,y;
    x = 42;
    y = 3;
    println_int(x * 2);
    println_int(2 * x);
    println_int((x * y) * 2);
    println_int(x * y * 2);
    return 0;
}

// EXPECTED
// 84
// 84
// 252
// 252

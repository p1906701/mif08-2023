#include "printlib.h"

int main() {
    bool x;
    int y;
    y = 42;
    x = true;
    if(x){
        println_bool(x);
        println_int(y);
    }
    return 0;
}

// EXPECTED
// 1
// 42

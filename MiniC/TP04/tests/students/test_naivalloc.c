#include "printlib.h"

int main() {
    int x,y,z;
    x = 42;
    y = 3; 
    z = x + y;
    println_int(x);
    println_int(y);
    println_int(z);
    return 0;
}

// EXPECTED
// 42
// 3
// 45

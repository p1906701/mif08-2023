#include "printlib.h"

int main() {
    
    int x,y;
    x = 42;
    y = 5;
    println_int(x % 2);
    println_int(x % y);
    println_int((x % 5) % y);
    println_int(x % 5 % y);
    return 0;
}

// EXPECTED
// 0
// 2
// 2
// 2

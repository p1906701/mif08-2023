#include "printlib.h"

int main() {
    int x,y;
    x = 5;
    println_int(x);
    if(x == 5)
    {
        y = 10;
        println_int(y);
    }
    println_int(x);
    return 0;
}

// EXPECTED
// 5
// 10
// 5


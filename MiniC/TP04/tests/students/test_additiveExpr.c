#include "printlib.h"

int main() {
    println_int(6-2);
    println_int(7-5*4);
    println_int(12-2);
    println_int(10-20);
    println_int((8-4)*2);
    println_int(8-4*2);
    println_int(-(6-8)*10); 
    return 0;
}

// EXPECTED
// 4
// -13
// 10
// -10
// 8
// 0
// 20
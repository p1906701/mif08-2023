#include "printlib.h"

int main() {
    
    int x,y;
    x = 42;
    y = 0;
    println_int(x % y);
    return 0;
}

// EXPECTED
// EXECCODE 1
// SKIP TEST EXPECTED
// Division by 0


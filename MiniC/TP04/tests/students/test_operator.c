#include "printlib.h"

int main() {
    int x,y;
    x = 42;
    y = 2;
    println_bool(x<y);
    println_bool(x<=y);
    println_bool(x<=x);
    println_bool(x>y);
    println_bool(x>=y);
    println_bool(x>=x);
    println_bool(x!=y);
    println_bool(x==y);
    return 0;
}

// EXPECTED
// 0
// 0
// 1
// 1
// 1
// 1
// 1
// 0


#include "printlib.h"

int main() {
    bool a,b;
    a = true;
    b = false;
    println_bool(a && a);
    println_bool(a && b);
    println_bool(b && b);
    return 0;
}

// EXPECTED
// 1
// 0
// 0

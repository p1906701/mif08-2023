#include "printlib.h"

int main() {
    
    int x,y;
    x = 42;
    y = 3;
    println_int(x / 2);
    println_int((x / 2) / y);
    println_int(x / 2 / y);
    return 0;
}

// EXPECTED
// 21
// 7
// 7

#include "printlib.h"

int main() {
    int a,b,c,d,e;
    a = 2;
    b = 5;
    c = 7;
    d = 8;
    e = 9;

    println_int(a + b * c);
    println_int(d - e * a);
    println_int(b * c - a);
    println_int(c - a * b);
    println_int(a * d - c);
    println_int((a + b) * c - d);
    println_int((d + e) * a - b * c);
    return 0;
}

// EXPECTED
// 37
// -10
// 33
// -3
// 9
// 41
// -1

#include "printlib.h"

int main() {
    bool x,y;
    x = true;
    y = false;
    println_bool(x);
    println_bool(y);
    x = !x;
    y = !y;
    println_bool(x);
    println_bool(y);
    return 0;
}

// EXPECTED
// 1
// 0
// 0
// 1

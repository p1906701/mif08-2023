#include "printlib.h"

int main()
{
  int a;
  a = 0;
  while (a < 10)
    a = a + 1;
  println_int(a);
  while (a > 100)
    a = 42;
  println_int(a);
  return 0;
}

// EXPECTED
// 10
// 10



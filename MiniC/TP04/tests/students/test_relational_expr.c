#include "printlib.h"

int main() {
    int x,y;
    x = 42;
    y = 2;
    if(x<y) 
        println_int(1);
    else 
        println_int(0);
    if(x<=y)
        println_int(1);
    else 
        println_int(0);
    if(x<=x)
        println_int(1);
    else 
        println_int(0);
    if(x>y)
        println_int(1);
    else 
        println_int(0);
    if(x>=y)
        println_int(1);
    else 
        println_int(0);
    if(x>=x)
        println_int(1);
    else 
        println_int(0);
    if(x!=y)
        println_int(1);
    else 
        println_int(0);
    if(x==y)
        println_int(1);
    else 
        println_int(0);
    return 0;
}

// EXPECTED
// 0
// 0
// 1
// 1
// 1
// 1
// 1
// 0


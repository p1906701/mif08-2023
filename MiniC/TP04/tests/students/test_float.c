#include "printlib.h"

int main() {
    float x;
    x = 42.2;
    println_float(x);
    return 0;
}

// EXITCODE 5
// EXPECTED
// Unsupported type float

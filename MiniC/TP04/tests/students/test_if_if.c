#include "printlib.h"

int main()
{
  int a,b;
  a = 10;
  b = -1; 
  if (a > 8) {
    if (b < 0)
      b = 1;
  }
  println_int(b);
  return 0;
}

// EXPECTED
// 1



from Lib import RiscV
from Lib.Operands import Temporary, Operand, S
from Lib.Statement import Instruction
from Lib.Allocator import Allocator
from typing import List, Dict


class AllInMemAllocator(Allocator):

    def replace(self, old_instr: Instruction) -> List[Instruction]:
        """Replace Temporary operands with the corresponding allocated
        memory location. FP points to the stack."""
        numreg = 1
        before: List[Instruction] = []
        after: List[Instruction] = []
        subst: Dict[Operand, Operand] = {}

        old_args = old_instr.args()  # récupère les arguments de l'instruction originale
        for arg in old_args:  # parcourt chaque argument de l'instruction originale
            if isinstance(arg, Temporary):  # vérifie si l'argument est un Temporary
                loc = arg.get_alloced_loc()  # obtient l'emplacement alloué pour le Temporary
                regxxx = S[numreg]  # obtient un registre de sauvegarde disponible pour stocker la valeur du Temporary
                numreg += 1  # incrémente le compteur de registres
                before.append(RiscV.ld(regxxx, loc))  # ajoute une instruction pour charger la valeur du Temporary dans le registre de sauvegarde
                after.append(RiscV.sd(regxxx, loc))  # ajoute une instruction pour stocker la valeur mise à jour du Temporary à son emplacement d'origine
                subst[arg] = regxxx  # remplace le Temporary par son emplacement de stockage dans le dictionnaire des substitutions



        new_instr = old_instr.substitute(subst)
        return before + [new_instr] + after

    def prepare(self) -> None:
        """Allocate all temporaries to memory.
        Invariants:
        - Expanded instructions can use s2 and s3
          (to store the values of temporaries before the actual instruction).
        """
        self._fdata._pool.set_temp_allocation(
            {temp: self._fdata.fresh_offset()
             for temp in self._fdata._pool.get_all_temps()})

#include "printlib.h"

int main()
{
  println_int( 20 % 11 );
  println_int( 100 % 10 );
  println_int( 4 % 2 );
  println_int( 19 % 18 );
  println_int( 6 % 1 );
  println_int( 8 % 6 );
  println_int( 50 % 25 );
  return 0;
}

// EXPECTED
// 9
// 0
// 0
// 1
// 0
// 2
// 0


#include "printlib.h"

int main()
{
  int a,b;
  a = 10;
  if (a > 8)
    b = 1;
  else
    b = 2;
  println_int(b);
  return 0;
}

// EXPECTED
// 1



#include "printlib.h"

int main()
{
  int a;
  a = 0;
  while (a < 10)
    a = a + 1;
  println_int(a);
  return 0;
}

// EXPECTED
// 10



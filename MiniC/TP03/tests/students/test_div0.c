#include "printlib.h"

int main()
{
  println_int( 20 / 0 );
  return 0;
}

// SKIP TEST EXPECTED
// EXECCODE 1
// EXPECTED
// Division by 0



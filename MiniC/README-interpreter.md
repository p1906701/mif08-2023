# MiniC interpreter and typer
LAB3, MIF08 / CAP / CS444 2022-23


# Authors

VINCENT Yann p1906701

# Contents

J'ai modifié le fichier MiniCinterpretVisitor.py afin qu'il fonctionne pour certaine fonction non implementer comme :
la declaration de liste de variable, la valeur apr defauts des variables, les assignation, les if / while etc
J'ai rajouter des test pour atteindre 100% de coverage


# Howto

`make test-interpret TEST_FILES='TP03/tests/provided/examples/test_print_int.c'` for a single run

`make test` to test all the files in `*/tests/*` according to `EXPECTED` results.

You can select the files you want to test by using `make test TEST_FILES='TP03/**/*bad*.c'` (`**` means
"any number of possibly nested directories").

# Test design 

J'ai rajouté 14 tests dans la partie students afin de couvrir 100% du fichier MiniCInterpretVisitor.py
2 tests sur la division : un pour la division par 0 et un pour la division de float
2 tests sur le modulo : 1 pour tester le mod 0 et 1 pour le modulo classique
3 tests sur la déclaration de variable : 1 pour les variable deja déclaré un pour les declarations de liste de variables
et un pour les variables non déclarées
7 tests sur les while et if else : des test pour tester les si les condition fonctionnent en les imbriquants entre elle en plus

# Design choices

j'ai placé mes tests dans le test/students comme demandé et j'ai implementé les fonctions demandées.

# Known bugs

Rien a signalé
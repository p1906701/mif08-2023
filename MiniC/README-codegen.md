# MiniC Compiler 
LAB4 (simple code generation), MIF08 / CAP 2022-23

# Authors

VINCENT Yann

# Contents

TODO for STUDENTS : Say a bit about the code infrastructure ...

# Test design 

TODO: explain your tests

# Design choices

TODO: explain your choices. How did you implement boolean not? Did you implement an extension?

# Known bugs

TODO: Bugs and limitations.

# Checklists

A check ([X]) means that the feature is implemented 
and *tested* with appropriate test cases.

## Code generation

- [X] Number Atom
- [X] Boolean Atom
- [X] Id Atom
- [X] Additive expression
- [X] Multiplicative expression
- [X] UnaryMinus expression
- [X] Or expression
- [X] And expression
- [X] Equality expression
- [X] Relational expression (! many cases -> many tests)
- [X] Not expression

## Statements

- [X] Prog, assignements
- [X] While
- [X] Cond Block
- [X] If
- [X] Nested ifs
- [X] Nested whiles

## Allocation

- [X] Naive allocation
- [X] All in memory allocation
- [X] Massive tests of memory allocation

